import dataclasses
import math
import typing

"""Permissible number of sides of a die"""
SIDES: typing.List[int] = [2, 4, 6, 8, 10, 12, 20, 30, 100]


@dataclasses.dataclass
class Solution:
    """
    Dice notation solution is of the form N'd'D x Q + C. For example, `3d6 + 2` has N = 3, D = 6, Q = 1 (implicit),
    and C = 2.
    """

    """N non-negative integer count of dice thrown"""
    N: int
    """D number of sides (faces) of the die thrown"""
    D: int
    """Q positive integer multiplier"""
    Q: int
    """C integer constant offset"""
    C: int

    def __str__(self) -> str:
        """
        Dice notation representation

        >>> str(Solution(3, 6, 1, 0))
        '3d6'
        >>> str(Solution(3, 6, 1, 4))
        '3d6+4'
        >>> str(Solution(3, 6, 2, 0))
        '3d6*2'
        >>> str(Solution(3, 6, 2, 4))
        '3d6*2+4'
        >>> str(Solution(0, 4, 1, 5))
        '+5'

        :return: dice notation representation
        """
        if self.N > 0:
            base = f'{self.N}d{self.D}'
        else:
            base = ''

        if self.Q == 1 and self.C == 0:  # implicit multiplication and addition
            return base
        elif self.Q == 1:  # implicit multiplication
            if self.C > 0:
                return f'{base}+{self.C}'
            else:
                return f'{base}{self.C}'
        elif self.C == 0:  # implicit addition
            return f'{base}*{self.Q}'
        else:  # full form
            if self.C > 0:
                return f'{base}*{self.Q}+{self.C}'
            else:
                return f'{base}*{self.Q}{self.C}'


def solve(a: int, b: int) -> typing.Iterable[Solution]:
    """

    >>> str(list(solve(1, 1))[0])
    '+1'
    >>> str(list(solve(1, 2))[0])
    '1d2'
    >>> list(map(str, solve(1, 3)))
    ['1d2*2-1', '2d2-1']
    >>> list(map(str, solve(6, 63)))
    ['1d2*57-51', '57d2-51', '3d2*19-51', '19d2*3-51', '1d4*19-13', '19d4-13', '1d20*3+3', '3d20+3']

    :param a: minimum value of range (inclusive)
    :param b: maximum value of range (inclusive)
    :return: list of solutions
    """

    assert isinstance(a, int), 'a must be an integer'
    assert isinstance(b, int), 'b must be an integer'
    assert a <= b, 'a must be less than or equal to b in a range'

    if a == b:  # short-cut for a single, constant solution
        yield Solution(0, 2, 1, a)
    else:
        # A roll of NdD * Q + C yields a minimal value of NQ + C (= a) and a maximal value of NQD + C (= b).
        # Let y = b - a (the range of the range)
        # Thus, y = b - a = NQD + C - NQ - C = NQD - NQ = NQ(D - 1)
        # Since we know NQ and y are integers, and the domain of D is small, we can filter to the set of valid NQs

        y = b - a
        potential_nqs = [y // (side - 1) for side in SIDES if y % (side - 1) == 0]
        for nq in potential_nqs:
            c = a - nq
            d = (b - c) // nq

            for n, q in _divisor_pairs(nq):
                if n != q:
                    # yield both n, q and q, n pairs since the solutions are distinct
                    yield Solution(n, d, q, c)
                    yield Solution(q, d, n, c)
                else:
                    yield Solution(n, d, q, c)


def _divisor_pairs(d: int) -> typing.Iterable[typing.Tuple[int, int]]:
    """
    Yield all (a, b) pairs such that a, b are positive integers, a <= b, and ab = d.

    >>> list(_divisor_pairs(1))
    [(1, 1)]
    >>> list(_divisor_pairs(2))
    [(1, 2)]
    >>> list(_divisor_pairs(4))
    [(1, 4), (2, 2)]
    >>> list(_divisor_pairs(12))
    [(1, 12), (2, 6), (3, 4)]

    :param d: a positive integer
    :return: an iterator of (a, b) tuples
    """
    assert isinstance(d, int), 'd must be an integer'
    assert d > 0, 'd must be positive'

    for a in range(1, math.floor(math.sqrt(d)) + 1):
        if d % a == 0:
            yield a, d // a
