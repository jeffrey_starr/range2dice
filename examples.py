import itertools
import range2dice

limit = 15

for a in range(1, 21):
    for b in range(a, 21):
        solutions = list(map(str, itertools.islice(range2dice.solve(a, b), limit + 1)))
        if len(solutions) > limit:
            solutions[-1] = '...'
        sol_text = ', '.join(solutions)
        print(f'({a}, {b}): {sol_text}')
