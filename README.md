

### Running tests

The program uses Python doctests. From the command line, you can run them via:

```commandline
python -m doctest -v range2dice/__init__.py
```
